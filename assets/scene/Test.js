
cc.Class({
    extends: cc.Component,

    properties: {
        spriteAnimate : require('SpriteAnimate'),
        atlas : cc.SpriteAtlas
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {

    },

    onPlayIdle(){
        this.spriteAnimate.loadFrames(this.atlas,'jie_',1,9);
        this.spriteAnimate.setDelayTime(0.1);
        this.spriteAnimate.startAnimate()
    },
    onPlayJie(){
        this.spriteAnimate.loadFrames(this.atlas,'shouwei_',1,6);
        this.spriteAnimate.setDelayTime(0.15);
        this.spriteAnimate.startAnimate()
    }


    // update (dt) {},
});
