const Loader = require('wy-Loader');
module.exports = {
    loadSence(senceName) {
        const m = cc.find('Canvas/Main');
        m.removeAllChildren();
        m.addChild(Loader.getInstantiate(`sence/${senceName}`));
    }
}