/* EjectManager的逻辑是
一层一层的向上叠加
*/

cc.Class({
    extends: cc.Component,

    properties: {
    },

    onLoad(){
        this.currentPanel = null;
        this.panelNames = [];

        this.panels = [];
    },

    clearNode(){
        this.node.removeAllChildren();
        this.panels = [];
        this.panelNames = [];
    },

    getTopUI(){
        return this.currentPanel;
    },

    pushUI(){
        var names = Array.prototype.slice.call(arguments);
        for(var i=0; i<names.length; i++){
            this.panelNames.push(names[i])
        }
        return this._updateEject();
    },

    popUI(){
        var self =this;

        if(this.currentPanel != null){
            this.currentPanel.emit('onExitWindowBegin');
            this.currentPanel.runAction(cc.sequence(cc.delayTime(0.3),cc.callFunc(function(node){
                node.emit('onExitWindowEnd');
                node.destroy();
            })))
            this.panels.pop();
        }
        if(this.panels.length >0){
            this.currentPanel = this.panels[this.panels.length-1];
            this.currentPanel.emit('onWindowFocus');
        } else {
            this.currentPanel = null
            global.UIMgr.postFocus();
        }
    },

    _updateEject(){
        if(this.panelNames.length > 0){
            var name = this.panelNames.pop();
            var node = global.Loader.getInstantiate(name)
            // setTimeout(()=>{ //延时一帧，保证所有方法都初始化完成
            //     node.emit('onEnterWindowBegin');
            // },10)
            this.scheduleOnce(()=>{
                node.emit('onEnterWindowBegin');
            },0.01)
            this.currentPanel = node;
            this.node.addChild(node);
            this.panels.push(node);
            this._updateEject();
            return node;
        }
        return null;
    }

});
