const Loader = require('wy-Loader');
let lodingObj;
let tipObj;
let UINode;
let isLoading = false;
module.exports = {
    //显示Loading转圈
    ShowLoadding: function () {
        isLoading = true;
        let preArr = ['./UI/UI-Waiting'];
        Loader.loadResourcesArray(preArr, () => {
            if (!lodingObj) {
                lodingObj = Loader.getInstantiate(preArr[0]);
                this._getUI().addChild(lodingObj);
            }
            lodingObj.active = isLoading;
        }, () => { });
    },
    //隐藏loading转圈
    HideLoadding: function () {
        isLoading = false;
        if (!!lodingObj)
            lodingObj.active = isLoading;
    },

    /**
     * 显示一个小提示
     * 参数options : {
     *     message: '这是一条提示信息！',
     *     time: 1.5,
     *     y: 100,
     * };
     */
    ToolTip: function (options) {
        let preArr = ['./UI/UI-Tooltip', './UI/UI-TooltipObj'];
        Loader.loadResourcesArray(preArr, () => {
            if (!tipObj) {
                tipObj = Loader.getInstantiate(preArr[0]);
                this._getUI().addChild(tipObj);
            }
            tipObj.js.setOptions(options);
        }, () => { });
    },

    /**
     * 显示单个弹框提醒
     * 参数options : {
     *     message: '随便写点什么显示出来就可以了，过程不重要！',
     *     title: 'alert组件',
     *     okButtonText: "确认",
     *     okButtonCallback: () => { cc.log('点击了确定！'); },
     * };
     */
    Alert: function (options) {
        let preArr = ['./UI/UI-Panel', './UI/UI-Alert', './UI/UI-TextButton'];
        Loader.loadResourcesArray(preArr, () => {
            let alert = Loader.getInstantiate(preArr[1]);
            this._getUI().addChild(alert);
            alert.js.setOptions(options);
        }, () => { });
    },

    /**
     * 显示二个按钮弹框
     * 参数options : {
     *     message: '随便写点什么显示出来就可以了，过程不重要！',
     *     title: 'confirm组件',
     *     okButtonText: "确认",
     *     cancelButtonText: "取消",
     *     okButtonCallback: () => { cc.log('点击了确定！'); },
     *     cancelButtonCallback: () => { cc.log('点击了取消！'); },
     * };
     */
    Confirm: function (options) {
        let preArr = ['./UI/UI-Panel', './UI/UI-Confirm', './UI/UI-TextButton'];
        Loader.loadResourcesArray(preArr, () => {
            let alert = Loader.getInstantiate(preArr[1]);
            this._getUI().addChild(alert);
            alert.js.setOptions(options);
        }, () => { });
    },

    /**
     * 显示 多功能 弹框
     * 参数options : {
     *     x: 0, y: 100,
     *     width: 600,
     *     height: 400,
     *     isClose: false,
     *     isModel: true,
     *     isModelClose: true,
     *     content: this is node,
     *     titleText: '温馨提示',
     * };
     */
    Panel: function (options) {
        let preArr = ['./UI/UI-Panel'];
        Loader.loadResourcesArray(preArr, () => {
            let panel = Loader.getInstantiate(preArr[0]);
            this._getUI().addChild(panel);
            panel.js.setOptions(options);
        }, () => { });
    },

    //获取UI层
    _getUI: function () {
        if (!UINode)
            UINode = cc.find('Canvas/Eject') || cc.find('Canvas');
        return UINode;
    },

}