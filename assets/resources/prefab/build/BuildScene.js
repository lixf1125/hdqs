
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        spriteAtlas : cc.SpriteAtlas
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.no_daoyu = this.node.getChildByName('daoyu')
        this.no_ship = this.node.getChildByName('ship');
        this.no_mask = this.no_daoyu.getChildByName('mask')
        this.no_back = this.node.getChildByName('back')

        this.toMinWindow(false);
        this.no_ship.active = false;

        // this.no_ship.getChildByName('model').getComponent(cc.Sprite)._sgNode.setState(1)
        var sp = this.no_ship.getChildByName('model').getComponent(cc.Sprite);
        global.isVersion2?sp.setState(1) : sp._sgNode.setState(1)
    },

    start(){
        this.updateLand();
    },

    registerTouch(){
        this.no_daoyu.getChildByName('fram').on(cc.Node.EventType.TOUCH_END,this.onBtnBuild.bind(this,'vegetableField'),this)
        this.no_daoyu.getChildByName('home').on(cc.Node.EventType.TOUCH_END,this.onBtnBuild.bind(this,'burrow'),this)
        this.no_daoyu.getChildByName('wood').on(cc.Node.EventType.TOUCH_END,this.onBtnBuild.bind(this,'lumberyard'),this)
        
        this.no_daoyu.getChildByName('quarry').on(cc.Node.EventType.TOUCH_END,this.onBtnBuild.bind(this,'quarry'),this)
        this.no_daoyu.getChildByName('fire').on(cc.Node.EventType.TOUCH_END,this.onBtnBuild.bind(this,'fire'),this)

        this.node.on(cc.Node.EventType.TOUCH_MOVE,this.onTouch,this);
    },
    unRegisterTouch(){
        this.node.off(cc.Node.EventType.TOUCH_MOVE,this.onTouch,this);
        this.no_daoyu.getChildByName('fram').targetOff(this);
        this.no_daoyu.getChildByName('home').targetOff(this);
        this.no_daoyu.getChildByName('wood').targetOff(this);
        this.no_daoyu.getChildByName('fire').targetOff(this);
        this.no_daoyu.getChildByName('quarry').targetOff(this);
    },

    onTouch(event){
        var start = event.getStartLocation();
        var current = event.getLocation();
        if(current.x > start.x + 10){
            global.GameLoop.toReadyLayer()
        }
    },

    toMinWindow(anim){
        if(!anim){
            this.no_daoyu.setScale(0.3)
            this.no_daoyu.opacity = 180;
            this.no_daoyu.x = 203;
            this.no_daoyu.y = 146;
            this.no_back.x = -459;
            this.no_ship.x = 100;
            this.no_ship.y = 283;
            this.no_ship.opacity = 180
        } else {
            this.no_ship.runAction(cc.spawn(cc.moveTo(0.5,cc.v2(100,283)),cc.fadeTo(0.5,180),cc.scaleTo(0.5,0.3)).easing(cc.easeIn(0.5)));
        
            this.no_daoyu.runAction(cc.spawn(cc.moveTo(0.5,cc.v2(203,146)),cc.fadeTo(0.5,180),cc.scaleTo(0.5,0.3)).easing(cc.easeIn(0.5)));
            this.no_back.runAction(cc.moveTo(0.2,cc.v2(-459,-444)));
        }
        this.no_mask.active = true;
    },

    getBuildConfig(id){
        var c = global.DataConfig.island[id][global.Data.island.level];
        var c1 = c.split(',')
        var d = '';
        c1[0] >0 && (d += '木材x'+c1[0]+' ');
        c1[1] >0 && (d += '矿石x'+c1[1]+' ');
        c1[2] >0 && (d += '火x'+c1[2]+' ');
        c1[3] >0 && (d += '水x'+c1[3] + '');

        if(id == 'vegetableField'){
            return {id:'vegetableField',title1:'农场',title2:'用于种植农作物，收获能量\n造船时必须保证农场是使用状态',number:' ',name:'使用菜地',desc:d}
        } else if(id == 'burrow'){
            return {id:'burrow',title1:'巢穴',title2:'巢穴用来储备和收集水',number:'拥有水:{0}'.format(global.Data.store.water),name:'使用菜地',desc:d}
        } else if(id == 'lumberyard'){
            return {id:'lumberyard',title1:'伐木场',title2:'用于存放木材',number:'拥有木材:{0}'.format(global.Data.store.wood),name:'使用菜地',desc:d}
        } else if(id == 'quarry'){
            return {id:'quarry',title1:'采矿场',title2:'用于存放矿石',number:'拥有矿石:{0}'.format(global.Data.store.mineral),name:'使用菜地',desc:d}
        } else if(id == 'fire'){
            return {id:'fire',title1:'火堆',title2:'用来存放火种',number:'拥有火种:{0}'.format(global.Data.store.fire),name:'使用菜地',desc:d}
        }
    },

    getCreateConfig(id){
        var d = global.DataConfig.island.ship[id][global.Data.island.level];

        var c = {
            route:'island.ship.build',part:id,use:d.split(',')
        }
        if(id == 'water'){
            c.title = '水资源'
        } else if(id == 'sail'){
            c.title = '帆'
        } else if(id == 'skeleton'){
            c.title = '骨架'
        } else if(id == 'shell'){
            c.title = '壳'
        } else if(id == 'deck'){
            c.title = '甲板'
        } 
        return c;
    },

    onBtnSelectShip(event,data){
        var island = global.Data.island
        if(!island.vegetableField.isBuild || !island.burrow.isBuild || !island.lumberyard || !island.quarry || !island.fire.isBuild){
            global.UI.ToolTip({message:'需要把岛上的建筑都完成，才可以造船'})
            return;
        }        

        if(data == 'water' && global.Data.island.ship.water){
            global.UI.ToolTip({message:'水已经建造'})
            return;
        }
        if(data == 'sail' && global.Data.island.ship.sail){
            global.UI.ToolTip({message:'帆已经建造'})
            return;
        }
        if(data == 'skeleton' && global.Data.island.ship.skeleton){
            global.UI.ToolTip({message:'骨架已经建造'})
            return;
        }
        if(data == 'shell' && global.Data.island.ship.shell){
            global.UI.ToolTip({message:'壳已经建造'})
            return;
        }
        if(data == 'deck' && global.Data.island.ship.deck){
            global.UI.ToolTip({message:'甲板已经建造'})
            return;
        }
        var config = this.getCreateConfig(data);
        var bc = global.EJMgr.pushUI('prefab/build/CreateShipConfirm');
        config.cb = this.onBtnCreateShip.bind(this,config);
        bc.js.setOptions(config)
    },

    onBtnCreateShip(config,route,buildId){
        if(config.part == 'water'){
            global.Data.store.water -= config.use[0]
        } else {
            global.Data.store.wood -= config.use[0]
            global.Data.store.mineral -= config.use[1]
            global.Data.store.fire -= config.use[2]
        }
        global.Event.emit('updateResource');
        // global.GameLoop.updateHeadResources();
        
        // this.
        var node = null;
        global.Data.island.ship[buildId] = true;
        if(buildId == 'water') {
            node = this.no_ship.getChildByName('water');
        } else if(buildId == 'sail') {
            node = this.no_ship.getChildByName('fan');
        } else if(buildId == 'shell') {
            node = this.no_ship.getChildByName('ke');
        } else if(buildId == 'skeleton') {
            node = this.no_ship.getChildByName('gujia');
        } else if(buildId == 'deck') {
            node = this.no_ship.getChildByName('jiaban');
        }  else {
            return;
        }
        node.runAction(cc.sequence(cc.blink(1.5,3),cc.callFunc(this.updateLand.bind(this))))
    },

    onBtnBuild(data){
        cc.log('onBtnBuild:',data)
        var config = this.getBuildConfig(data);
        var childs = {vegetableField:'fram',burrow:'home',quarry:'quarry',lumberyard:'wood',fire:'fire'}

        var node = this.no_daoyu.getChildByName(childs[data])
        config.iconsf = node.getComponent(cc.Sprite).spriteFrame

        if(data == 'vegetableField' && global.Data.island.vegetableField.isBuild){
            var framNode = global.UIMgr.pushUI('prefab/build/FramLayer');
            framNode.getComponent('FramLayer').setOptions({_id:global.Data._id,url:global.Data.headImgUrl,nickName:global.Data.nickname})
            return;
        }
        if(data == 'burrow' && global.Data.island.burrow.isBuild){
            SoundMgr.playSound('prefab/build/sound/home')
            global.UI.ToolTip({message:'巢穴已经建造完成'})
            return;
        }
        if(data == 'lumberyard' && global.Data.island.lumberyard){
            SoundMgr.playSound('prefab/build/sound/wood')
            global.UI.ToolTip({message:'伐木场已经建造完成'})
            return;
        }
        if(data == 'quarry' && global.Data.island.quarry){
            SoundMgr.playSound('prefab/build/sound/quarry')
            global.UI.ToolTip({message:'采矿场已经建造完成'})
            return;
        }
        if(data == 'fire' && global.Data.island.fire.isBuild){
            SoundMgr.playSound('prefab/build/sound/fire')
            global.UI.ToolTip({message:'火堆已经建造完成'})
            return;
        }
        config.cb = this.onRespBuild.bind(this);

        var bc = global.EJMgr.pushUI('prefab/build/BuildConfirm');
        bc.js.setOptions(config)
    },

    onRespBuild(route,buildId){
        var c = global.DataConfig.island[buildId][global.Data.island.level];
        var d = c.split(',')
        global.Data.store.wood -= d[0];
        global.Data.store.mineral -= d[1];
        global.Data.store.fire -= d[2];
        global.Data.store.water -= d[3];
        global.Event.emit('updateResource');
        // global.GameLoop.updateHeadResources();

        var node = null;
        if(buildId == 'vegetableField') {
            global.Data.island.vegetableField.isBuild = true;
            node = this.no_daoyu.getChildByName('fram');
        } else if(buildId == 'burrow') {
            global.Data.island.burrow.isBuild = true;
            node = this.no_daoyu.getChildByName('home');
        } else if(buildId == 'lumberyard') {
            global.Data.island.lumberyard = true;
            node = this.no_daoyu.getChildByName('wood');
        } else if(buildId == 'quarry') {
            global.Data.island.quarry = true;
            node = this.no_daoyu.getChildByName('quarry');
        } else if(buildId == 'fire') {
            global.Data.island.fire.isBuild = true;
            node = this.no_daoyu.getChildByName('fire');
        }  else {
            return;
        }
        node.runAction(cc.sequence(cc.blink(1.5,3),cc.callFunc(this.updateLand.bind(this))))
    },

    updateLand(){
        var level = global.Data.island.level+1;
        this.no_daoyu.getChildByName('fram').getComponent(cc.Sprite).spriteFrame = this.spriteAtlas.getSpriteFrame('fram'+(global.Data.island.vegetableField.isBuild?1:0));
        this.no_daoyu.getChildByName('home').getComponent(cc.Sprite).spriteFrame = this.spriteAtlas.getSpriteFrame('home'+(global.Data.island.burrow.isBuild?level:0));
        var fire2 = this.spriteAtlas.getSpriteFrame('fire'+(global.Data.island.fire.isBuild?1:0))
        this.no_daoyu.getChildByName('fire').getComponent(cc.Sprite).spriteFrame = fire2
        this.no_daoyu.getChildByName('quarry').getComponent(cc.Sprite).spriteFrame = this.spriteAtlas.getSpriteFrame('quarry'+(global.Data.island.quarry?1:0));
        this.no_daoyu.getChildByName('wood').getComponent(cc.Sprite).spriteFrame = this.spriteAtlas.getSpriteFrame('wood'+(global.Data.island.lumberyard?1:0));
        
        var modelshow = global.Data.island.ship.water || global.Data.island.ship.sail || global.Data.island.ship.shell || global.Data.island.ship.deck ||global.Data.island.ship.skeleton
        var sp = this.no_ship.getChildByName('model').getComponent(cc.Sprite)
        global.isVersion2?sp.setState(modelshow?0:1) : sp._sgNode.setState(modelshow?0:1)

        // this.no_ship.getChildByName('model').getComponent(cc.Sprite).setState(modelshow?0:1)
       /* var home = this.no_daoyu.getChildByName('home').getComponent(cc.Sprite)
        home._sgNode.setState(global.Data.island.burrow.isBuild?0:1)
        home.spriteFrame = this.spriteAtlas.getSpriteFrame('home'+level);

        var fire = this.no_daoyu.getChildByName('fire').getComponent(cc.Sprite)
        fire._sgNode.setState(global.Data.island.fire.isBuild?0:1)
        fire.spriteFrame = this.spriteAtlas.getSpriteFrame('fire'+level);

        var quarry = this.no_daoyu.getChildByName('quarry').getComponent(cc.Sprite)
        quarry._sgNode.setState(global.Data.island.quarry?0:1)
        quarry.spriteFrame = this.spriteAtlas.getSpriteFrame('quarry'+level);

        var wood = this.no_daoyu.getChildByName('wood').getComponent(cc.Sprite)
        wood._sgNode.setState(global.Data.island.lumberyard?0:1)
        wood.spriteFrame = this.spriteAtlas.getSpriteFrame('wood'+level);*/

        var water = cc.find('water',this.no_ship).getComponent('UI-Toggle')
        water.setToggle(global.Data.island.ship.water)

        var water = cc.find('fan',this.no_ship).getComponent('UI-Toggle')
        water.setToggle(global.Data.island.ship.sail)

        var water = cc.find('ke',this.no_ship).getComponent('UI-Toggle')
        water.setToggle(global.Data.island.ship.shell)

        var water = cc.find('jiaban',this.no_ship).getComponent('UI-Toggle')
        water.setToggle(global.Data.island.ship.deck)

        var water = cc.find('gujia',this.no_ship).getComponent('UI-Toggle')
        water.setToggle(global.Data.island.ship.skeleton)

        if(global.Data.island.ship.water && global.Data.island.ship.sail && global.Data.island.ship.shell && global.Data.island.ship.deck && global.Data.island.ship.skeleton){
            // cc.log('所有建筑已经造好')

            global.UI.Alert({title:'提示',message:'所有建筑已经造好',okButtonCallback:function(){
                // global.GameLoop.toReadyLayer();
                cc.game.restart();
            }})
        }
    },

    onBtnBackMainScene(){
        global.GameLoop.toReadyLayer();
    },

    onEnterMainScene(){
        this.stopActions();
        this.no_daoyu.runAction(cc.spawn(cc.moveTo(0.5,cc.v2(0,-158)),cc.fadeIn(0.5),cc.scaleTo(0.5,1.0)).easing(cc.easeIn(0.5)));
        this.no_back.runAction(cc.moveTo(0.2,cc.v2(-259,-444)));
        this.no_ship.active = true;
        this.no_ship.runAction(cc.spawn(cc.moveTo(0.5,cc.v2(0,214)),cc.fadeIn(0.5),cc.scaleTo(0.5,1.0)).easing(cc.easeIn(0.5)));
        
        this.no_mask.active = false;
        this.registerTouch()
        
        var shipItem = ['water','ke','fan','jiaban','gujia']
        for(var i=0; i<5; i++){
            var item = cc.find('ship/'+shipItem[i],this.node)
            item.opacity = 0;
            item.runAction(cc.sequence(cc.delayTime(0.4+i*0.1),cc.fadeIn(0.1)));
        }
        var self = this;
        var async = new CCAsync();
        async.parallel([function(cb){
            self.node.runAction(cc.sequence(cc.delayTime(0.4),cc.callFunc(function(){
                cb(null,1)
            })))
        },function(cb){
            global.Http.get('/game/gameuser','island.ship.getCurrentShipMessage',{}, cb);
        }],function(err,res){
            if(res){
                global.Data.island.ship = res[1].ship;
            }
            self.updateLand();
            // cc.log('动画完成:',res);
        })
    },

    onExitMainScene(){
        this.stopActions();
        this.toMinWindow(true);
        var shipItem = ['ke','fan','water','jiaban','gujia']
        this.node.getChildByName('ship').active = true;
        for(var i=4; i>=0; i--){
            var item = cc.find('ship/'+shipItem[i],this.node)
            item.runAction(cc.sequence(cc.delayTime(i*0.1),cc.fadeOut(0.1)));
        }
        this.unRegisterTouch()
    },
    stopActions(){
        cc.log('bulid')
        var shipItem = ['ke','fan','water','jiaban','gujia']
        for(var i=4; i>=0; i--){
            var item = cc.find('ship/'+shipItem[i],this.node)
            item.stopAllActions();
        }
        this.no_daoyu.stopAllActions();
        this.no_back.stopAllActions();
        this.no_ship.stopAllActions();
    },
    onEnterWindowBegin(){
        this._super();
        this.registerTouch()
    }


    // update (dt) {},
});
