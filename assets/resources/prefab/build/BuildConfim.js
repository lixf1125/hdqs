
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
    },

    onLoad () {
        this._super();
        this.lb_title1 = cc.find('view/title1',this.node).getComponent(cc.Label)
        this.lb_title2 = cc.find('view/title2',this.node).getComponent(cc.Label)
        this.sp_icon = cc.find('view/icon/icon',this.node).getComponent(cc.Sprite)
        this.lb_ower = cc.find('view/number',this.node).getComponent(cc.Label)
        this.lb_btnName = cc.find('view/build/name',this.node).getComponent(cc.Label)
        this.lb_btnDesc = cc.find('view/build/desc',this.node).getComponent(cc.Label)
    },

    onBtnConfirm(){
        var obj = {};
        !!this.buildId && (obj.part = this.buildId);
        var self = this;
        global.Http.get('/game/gameuser','island.build',obj, function(err,res){
            if(err){
                cc.log('建造失败')
                return;
            }
            self.cb_Build&&self.cb_Build(self.route,self.buildId);
            global.EJMgr.popUI();
        })
    },

    setOptions(options){
        options.id&&(this.buildId = options.id);
        options.title1&&(this.lb_title1.string = options.title1);
        options.title2&&(this.lb_title2.string = options.title2);
        options.icon&&(this.sp_icon.spriteFrame = options.icon);
        options.number&&(this.lb_ower.string = options.number);
        options.desc&&(this.lb_btnDesc.string = options.desc);
        options.cb&&(this.cb_Build = options.cb);
        options.route&&(this.route = options.route)
        this.sp_icon.spriteFrame = options.iconsf
    }

});
