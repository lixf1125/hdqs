
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
    },

    start(){
        this.node.getChildByName('icon').js.setOptions({url:global.Data.headImgUrl})
    },

    onClickAvatar(){
        cc.log('onClickAvatar')
        global.UIMgr.pushUI('UI/leftMenu/LeftMenu')
    }
});
