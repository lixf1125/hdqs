
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
    },
    onLoad(){
        this._super();
        this.node.opacity = 0;
        this.n_runTime = 3;
        this.n_chatIndex = 0;
        this.n_lastTime = 0;
        this.lb_minLabel = cc.find('mask/label',this.node).getComponent(cc.Label)
        global.Event.on('requestWorldMessage',this.requestWorldMessage.bind(this));
    },
    onDestroy(){
        global.Event.off('requestWorldMessage');
    },
    onBtnMessage(){
        // global.UIMgr.pushUI('UI/chat/ChatLayer')
        global.UIMgr.loadLayer('Chat','UI/chat/ChatLayer',2);
    },

    updateMessage(){
        var time = new Date().getTime();
        var afterIndex = 0;
        while(afterIndex < global.ChatMsgQueue.length){
            var obj_msg = global.ChatMsgQueue[afterIndex];
            if(obj_msg.minshow && time - obj_msg.time > 30*60*1000){
                global.ChatMsgQueue.shift();
            }
            if(!obj_msg.minshow && afterIndex == global.ChatMsgQueue.length-1){
                obj_msg.minshow = true;
                this.lb_minLabel.string = obj_msg.nickname + ':'+ (obj_msg.type == 1? '[招募信息]': obj_msg.content);
            }
            afterIndex++;
        }
    },
    update(dt){
        this.n_runTime += dt;
        if(this.n_runTime >= 3.0){
            this.n_runTime = 0;
            if(CC_WECHATGAME){
                this.requestWorldMessage();
            }
            // 
        }
    },

    requestWorldMessage(){
        var self = this;
        this.httpGet('/game/gameUser/chat','newest',{channel:'world',lastTime:this.n_lastTime},(err,res)=>{
            if(err || res.length == 0){
                return
            }
            global.ChatMsgQueue = global.ChatMsgQueue.concat(res);
            global.Event.emit('newchatmsg');
            if(global.ChatMsgQueue.length>0){
                self.n_lastTime = global.ChatMsgQueue[global.ChatMsgQueue.length-1].time;
            }
            this.updateMessage();
        })

    },

    onEnterWindowBegin(){
        this.node.runAction(cc.sequence(cc.delayTime(0.2),cc.fadeIn(0.3)));
    },

    onExitWindowBegin(){
        this.node.runAction(cc.fadeOut(0.3));
    }


    
});
