
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
    },

    onLoad(){
        this._super()
        this.node.getChildByName('guang2').runAction(cc.repeatForever(cc.rotateBy(2.0,-180)));
        for(var i=0; i<this.node.children.length; i++){
            var node = this.node.children[i];
            if(node.name != 'bg' && node.name != 'icon')
                node.opacity = 0;
        }
    },

    start(){
        this.node.runAction(cc.sequence(cc.delayTime(1.5),cc.callFunc(()=>{
            global.EJMgr.popUI();
        })))
    },

    setOptions(options){
        var icon = this.node.getChildByName('icon')
        icon.getComponent(cc.Sprite).spriteFrame = options.sf;
        this.options = options;
        if(options.startPos){
            icon.setPosition(this.node.convertToNodeSpace(options.startPos));
        }
        
        icon.scaleX = 1;
        icon.scaleY = 1
        icon.runAction(cc.sequence(cc.scaleTo(0.15,2.0),cc.moveTo(0.15,cc.v2(0,-50))))
        this.node.getChildByName('name').getComponent(cc.Label).string = options.name
        this.exitCB = options.exitCB;
    },

    onEnterWindowBegin(){
        for(var i=0; i<this.node.children.length; i++){
            var node = this.node.children[i];
            if(node.name != 'icon'){
                if(node.name == 'bg'){
                    node.runAction(cc.sequence(cc.delayTime(0.3),cc.fadeTo(0.15,180)))
                } else {
                    node.runAction(cc.sequence(cc.delayTime(0.3),cc.fadeIn(0.15)))
                }
            }  
        }
    },
    onExitWindowBegin(){
        var icon = this.node.getChildByName('icon');
        if(this.options.endPos){
            icon.runAction(cc.sequence(cc.spawn(cc.moveTo(0.2,this.options.endPos),cc.scaleTo(0.2,1.0)),this.getExitAction()));
        } else {
            icon.runAction(cc.sequence(cc.delayTime(0.2),this.getExitAction()));
        }

        for(var i=0; i<this.node.children.length; i++){
            var node = this.node.children[i];
            if(node.name != 'icon'){
                node.runAction(cc.fadeOut(0.15))
            }  
        }
    },
    onExitWindowEnd(){
        global.EJMgr.popUI();
        this.exitCB&& this.exitCB();
    }
    
});
