
cc.Class({
    extends: require('wy-Component'),

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.js_icon = cc.find('headInfo/icon',this.node).getComponent('ImageLoader');
        this.lb_name = cc.find('headInfo/name',this.node).getComponent(cc.Label);
        this.lb_level = cc.find('headInfo/level',this.node).getComponent(cc.Label);
        this.js_sex = cc.find('headInfo/sex',this.node).getComponent('UI-Toggle');
    },

    start () {

    },

    onBtnFriend(event,data){
        var self = this;
        this.httpGet('/game/gameuser','sociality.getFriendsList',{}, function(err,res){
            if(!err){
                var node = global.EJMgr.pushUI('prefab/mutual/FramFriends');
                node.getComponent('FramFriends').setOptions(res.friends,'好友列表',self.onBtnGotoFriend.bind(self) );
            }
        });
    },

    onBtnEnemy(event,data){
        var self = this;
        this.httpGet('/game/gameuser','sociality.getEnemyList',{}, function(err,res){
            if(!err){
                var node = global.EJMgr.pushUI('prefab/mutual/FramFriends');
                node.getComponent('FramFriends').setOptions(res.enemy,'仇人列表',self.onBtnGotoFriend.bind(self) );
            }
        });
    },

    onBtnGotoFriend(_id){
        this.httpGet('/game/gameuser','island.ship.troublemaker',{_id:_id}, (err,res)=>{
            if(!err){
                this.setOptions(res);
            }
        })
    },

    onBtnSelect(event,data){
        cc.log('data:',data);

        this.httpGet('/game/gameuser','island.ship.destroyShip',{_id:this.option._id,part:data}, function(err,res){
            cc.log('res:',res);
            global.GameLoop.mutualToMainLayer();
        })
        
        //stealIsland
        // global.GameLoop.mutualToMainLayer();
    },

    updateUserInfo(){
        this.js_icon.setOptions({url:this.option.headImgUrl})
        this.lb_name.string = this.option.nickname;
        this.lb_level.string = this.option.civilizationLevel;
        this.js_sex.setToggle(this.option.sex);
        cc.find('ship/water',this.node).getComponent(cc.Button).interactable = this.option.ship.water;
        cc.find('ship/ke',this.node).getComponent(cc.Button).interactable = this.option.ship.shell;
        cc.find('ship/jiaban',this.node).getComponent(cc.Button).interactable = this.option.ship.deck;
        cc.find('ship/fan',this.node).getComponent(cc.Button).interactable = this.option.ship.sail;
        cc.find('ship/gujia',this.node).getComponent(cc.Button).interactable = this.option.ship.skeleton;

        if(!this.option.ship.water && !this.option.ship.shell && !this.option.ship.deck && !this.option.ship.sail && !this.option.ship.skeleton)
            global.UI.Alert({title:'提示',message:'没有可捣乱的位置',okButtonCallback:function(){
                global.GameLoop.mutualToMainLayer();
            }}
        )
    },

    setOptions(option){
        this.option = option;
        this.updateUserInfo();
    }

    // update (dt) {},
});
