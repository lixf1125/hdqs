
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        btnText : cc.Label
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    setOptions(options){
        if(options.user){
            var user = cc.find('views/user',this.node);
            user.active = true;
            user.getComponent('UserInfo').setOptions(options.user);
            this.btnText.string = '领取'
        } else {
            var other = cc.find('views/other',this.node);
            other.active = true;
            other.getComponent(cc.Label).string ='打开后，原来是个海星';
            this.btnText.string = '放回海里'
        }
        
    }

    // update (dt) {},
});
