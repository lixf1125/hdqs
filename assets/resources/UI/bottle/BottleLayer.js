
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        renButton : require('CM-TagButton'),
        jianButton : require('CM-TagButton')
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._super();
        this.node.x = -this.node.width
    },


    start () {

        // var bottle = global.Data.driftBottle.discardCount+global.Data.driftBottle.pickupCount
        this.updateTag();
    },

    updateTag(){
        this.renButton.setOptions({tag:global.Data.driftBottle.discardCount>0,text:global.Data.driftBottle.discardCount})
        this.jianButton.setOptions({tag:global.Data.driftBottle.pickupCount>0,text:global.Data.driftBottle.pickupCount})
    },

    onBtnGet(){
        if(global.Data.driftBottle.pickupCount==0){
            global.UI.ToolTip({message:'阁下捡瓶子的次数不足'})
            return
        }
        var self = this;
        this.httpGet('/game/gameUser','sociality.driftBottle.pickup',{goodsId:this.selectIndex},function(err,res){
            global.Data.driftBottle.pickupCount--;
            self.updateTag();
            var node = global.EJMgr.pushUI('UI/bottle/BottleResult');
            node.getComponent('BottleResult').setOptions(res);
            // cc.log('')
        })
        
    },
    onBtnWish(){
        global.EJMgr.pushUI('UI/bottle/BottleSelect')
    },
    onWindowFocus(){
        this.updateTag();
    },
    onEnterWindowBegin(){
        this.node.runAction(cc.moveTo(0.3,cc.v2(0,0)))
    },
    onExitWindowBegin(){
        this.node.runAction(cc.moveTo(0.3,cc.v2(-this.node.width,0)))
    }

    // update (dt) {},
});
