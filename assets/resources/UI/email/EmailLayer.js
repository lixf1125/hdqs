
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        msgScrollView :cc.ScrollView,
        emailScrollView :cc.ScrollView,
        emailItem :cc.Prefab,
        msgItem : cc.Prefab,
        tip:cc.Label,
        message:cc.Node,
        email:cc.Node
    },
    onLoad(){
        this.onTouchMessage();
        this.message.on(cc.Node.EventType.TOUCH_END,this.onTouchMessage,this);
        this.email.on(cc.Node.EventType.TOUCH_END,this.onTouchEmail,this);
    },
    onTouchMessage(){
        this.tip.string='消息只保留24小时'
    },
    onTouchEmail(){
        this.tip.string='邮件只保留7天'
    }
/*
    onLoad () {
        this._super();
        this.initWindowSize();
        // this.n_channel = 'guild'+global.Data.id

    },
    start(){
        this.o_messages = [];
        this.o_emails = [];
        this.n_firstTime = null;
        this.n_lastTime = null;
        this.emailPage = 1;
        this.msgScrollView.content.removeAllChildren();
        this.emailScrollView.content.removeAllChildren();

        // this.scorllToTop();
    },
    setChannel(c){
        this.n_channel = c
    },

    onEnable(){
        this._super();
        this.msgScrollView.node.on("bounce-top", this.scorllToTop, this);
        this.emailScrollView.node.on("bounce-top", this.scorllToTop, this);
        // this.scrollView.node.on("bounce-bottom", this.scrollToBottom, this);
    },
    onDisable(){
        this.msgScrollView.node.off("bounce-top",);
        this.emailScrollView.node.off("bounce-top",);
        // this.scrollView.node.off("bounce-bottom");
    },
    // scrollToBottom(){
    //     this.newMsgTag.active = false
    //     this.updateMessage();
    // },

    scorllToTop(){
        var self = this;
        this.httpGet('/game/gameUser','email.list',{page:this.emailPage},(err,res)=>{
            if(err){
                return
            }
            self.emailPage++;
            for(var i=0; i<res.emails.length; i++){
                if(res.emails[i].type < 10){
                    self.o_emails.push(res.emails[i]);
                } else {
                    self.o_messages.push(res.emails[i]);
                }
            }
            self.addMessage();
            self.addEmails();
            // this.addMessageToBottom(res);
        })
    },

    addMessage(){
        
    },
    
    addEmails(){
        // var d = new Date().format('yyyy-MM-dd hh:mm:ss')
        for(var i=0; i<this.o_emails.length; i++){
            var email =  this.o_emails[i];
            if(!email.isShow) {
                var node = cc.instantiate(this.emailItem);
                node.__id = email._id
                cc.find('info/title',node).getComponent(cc.Label).string = email.title;
                cc.find('info/time',node).getComponent(cc.Label).string = new Date(email.addTick).format('yyyy-MM-dd hh:mm:ss');
                cc.find('other/content/message',node).getComponent(cc.Label).string = email.content;
                var close = cc.find('other/close',node);
                close.show = false;
                global.Common.addButtonHandler(this,close,'onBtnCloseEmail',email._id);
                var get = cc.find('info/get',node).getComponent(cc.Button);
                if(email.status != 2){
                    global.Common.addButtonHandler(this,get.node,'onBtnReadEmail',email._id);
                } else {
                    get.interactable = false;
                }
                
                this.emailScrollView.content.addChild(node);
                email.isShow = true;
            }
        }
    },

    onBtnGetProp(event,eid){
        this.httpGet('/game/gameUser','email.read',{e_id:eid},function(err,res){})
    },

    onBtnCloseEmail(event,eid){
        for(var i=0; i<this.emailScrollView.content.children.length; i++){
            var node = this.emailScrollView.content.children[i]
            if(eid == node.__id){
                node.getChildByName('other').active = false;
            }
        }
        // event.target.parent.getChildByName('content').active = true;
    },

    onBtnReadEmail(event,eid){
        var btn = event.target
        var _node = null;
        for(var i=0; i<this.emailScrollView.content.children.length; i++){
            var node = this.emailScrollView.content.children[i]
            if(eid == node.__id){
                _node = node;
                node.getChildByName('other').active = true;
            }
        }

        this.httpGet('/game/gameUser','email.read',{e_id:eid},function(err,res){
            // var get = cc.find('info/get',_node).getComponent(cc.Button);
            // get.interactable = false;
        })

        for(var i=0; i<this.o_emails.length; i++){
            var email =   this.o_emails[i];
            if(email._id == eid){
                if(email.status == 0){
                    email.status = 1;
                    
                }
                break;
            }
        }
    },


    
    updateMessage(){
        this.httpGet('/game/gameUser/chat','newest',{channel:this.n_channel,lastTime:this.n_lastTime},(err,res)=>{
            if(err){
                return
            }
            if(res.length >0){
                this.n_lastTime = res[res.length-1].time
                this.messages = this.messages.concat(res)
                this.addMessageToBottom();
            } else {
                this.scrollView.node.off("bounce-top", this.scorllToTop, this);
            }
        })
    },

    sendObject(obj,cb){
        Object.assign(obj,{channel:this.n_channel})
        var msg = this.editText.string;
        // this.httpGet('/game/gameUser/chat','send',{channel:this.n_channel,type:0,context:msg},cb)
        this.httpGet('/game/gameUser/chat','send',obj,cb)
        
    },

    onPageShow(){
        this.updateMessage()
    }*/
});
