
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        image : require('ImageLoader'),
        scrollView : cc.ScrollView,
        lbPrefab : cc.Prefab,
        btnLabel : cc.Label
    },

    onLoad () {
        this._super();
    },

    start () {
        // this.node.on(cc.Node.EventType.TOUCH_START,this.onTouch,this)
        // var anniu = cc.find('Canvas/UI/anniu');
        // var panel = this.node.getChildByName('panel');
        // var point = anniu.convertToWorldSpaceAR(cc.v2(0,0))
        // var p = this.node.convertToNodeSpaceAR(point)
        // panel.x = p.x
        // panel.y = p.y
        // panel.width = anniu.width
        // panel.height = anniu.height
        // this.bounding = panel.getBoundingBox()
        this.updateNotice();
    },
    updateNotice(){
        this.scrollView.content.removeAllChildren();
        var notice = global.Data.sysNotices.shift();
        // this.image
        this.image.setOptions({url:notice.aboutImageUrl})
        var msg = notice.content.split('\n')
        for(var i=0; i<msg.length; i++){
            var pb = cc.instantiate(this.lbPrefab);
            pb.getComponent(cc.Label).string = global.Common.subSectionString(msg[i],35);
            this.scrollView.content.addChild(pb);
        }
        this.btnLabel.string = global.Data.sysNotices.length == 0 ? '确  认' : '下一条'
    },

    onBtnNext(){
        if(global.Data.sysNotices.length){
            this.updateNotice();
        } else {
            global.EJMgr.popUI()
        }
    },


    onTouch(event){
        var pos = event.getLocation();
        var p1 = this.node.convertToNodeSpaceAR(pos);
        var p = this.bounding.contains(p1);
        if(!p){
            event.stopPropagation();
        }
    }

    // update (dt) {},
});
