
cc.Class({
    extends: require('wy-Component'),

    properties: {
    },

    //由关闭上层界面的方式  激活子界面
    onWindowFocus(){
        cc.log('onWindowFocus login')
    },
    //切换pageview时关闭子界面
    onPageHide(){
        cc.log('onPageHide login')
    },
    //切换pageview时显示子界面
    onPageShow(){
        cc.log('onPageShow login')
    }
});
