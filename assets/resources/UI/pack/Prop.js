
cc.Class({
    extends: cc.Component,

    properties: {
        frames : cc.SpriteAtlas,
        icon : cc.Sprite,
        idLabel : cc.Label,
        nameLabel :cc.Label,
        descLabel : cc.Label,
        numLabel : cc.Label,
        defaultId : 0
    },
    start(){
        if(this.id == null){
            this.setId(this.defaultId);
        }
    },
    setId(id,num){
        this.id = id;
        var item = global.DataConfig.storage[id]
        this.idLabel && (this.idLabel.string = item.id);
        this.nameLabel && (this.nameLabel.string = item.name);
        this.descLabel && (this.descLabel.string = item.desc);
        this.numLabel && num != null && (this.numLabel.string = num);

        cc.loader.loadRes('UI/pack/img/'+item.img,cc.SpriteFrame,(err,res)=>{
            if(err){
                return console.warn('没有找到道具图片',global.DataConfig.storage[id])
            }
            this.icon.spriteFrame = res
        })
        // var sf = this.frames.getSpriteFrame(item.img);
        
        
    },
    getId(){
        return this.id
    },

    getConfig(id){
        var i = id ? id : this.id;
        return global.DataConfig.storage[i]
    },
    getSpriteFrame(){
        return this.icon.spriteFrame
    }
});
