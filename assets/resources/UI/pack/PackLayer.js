
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        PackItem : cc.Prefab,
        content : cc.Node,
        scrollView : cc.ScrollView
        
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._super();
    },

    addItems(){
        var pklen = this.packData.length
        for(var i=0; i<pklen; i++){
            var prop = this.packData[i]
            
            var item = cc.instantiate(this.PackItem);
            var itemconfig = global.DataConfig.storage[prop.id];
            if(!itemconfig){
                console.warn('没有找到这个道具的配置:',prop.id)
                global.UI.ToolTip({message:'没有找到这个道具的配置:'+prop.id})
                return;
            }
            this.content.addChild(item);
            item.getComponent('Prop').setId(prop.id,prop.count)
            // item.getChildByName('count').getComponent(cc.Label).string = prop.count;
            // item.getChildByName('name').getComponent(cc.Label).string = itemconfig.name;
            // item.getChildByName('desc').getComponent(cc.Label).string = itemconfig.desc;
            item.x = -550
            item.runAction(cc.sequence(cc.delayTime(0.2+i*0.1),cc.moveTo(0.2,cc.v2(0,0))))
        }

        var array = {power:0,water:1,corn:2,fire:3,mineral:4,potato:5,soy:7,wood:9,sorghum:8};
        
        for(var key in array){
            var v= array[key];
            var count = global.Data.store[key];

            var item = cc.instantiate(this.PackItem);
            this.content.addChild(item);
            item.getComponent('Prop').setId(v,count)
            item.getChildByName('use').active = false;
            item.x = -550
            item.runAction(cc.sequence(cc.delayTime(0.2+i*0.1),cc.moveTo(0.2,cc.v2(0,0))))
            i++
        }
    },

    onEnterWindowBegin(){
        var self = this;
        var async = new CCAsync();
        async.parallel([function(cb){
            var f = true;
            for(var i=0; i<self.node.children.length; i++){
                var chd = self.node.children[i];
                if(chd.name == 'bg' || chd.name == 'mask'){
                    chd.runAction(cc.fadeTo(0.15,150));
                } else {
                    var actions = self.getEjectAction();
                    if(f){
                        actions.push(cc.callFunc(function(){
                            cb(null,1)
                        }))
                        f = false;
                    }
                    
                    chd.runAction(cc.sequence(actions))
                    chd.runAction(cc.fadeIn(0.2));
                }
            }
        },function(cb){
            self.httpGet('/game/gameuser','getBagMessage',{}, cb);    
        }],function(err,result){
            if(err){
                global.UI.ToolTip({message:'加载失败'+JSON.stringify(err)});
                return;
            }
            self.packData = result[1].bag;
            global.Data.store = result[1].store;
            self.addItems();
            cc.log('result:',result);
        })
    },

    onEnterWindowEnd(){

    }

    // update (dt) {},
});
