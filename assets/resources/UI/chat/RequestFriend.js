
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        requestItem : cc.Prefab,
        content :cc.Node,
        myCode : cc.Label,
        editbox : cc.EditBox
    },

    onLoad(){
        this._super();
        this.myCode.string = global.Data.id;
    },

    onBtnCopyCode(){
        
    },
    onBtnAddFriend(){
        var code = this.editbox.string
        this.httpGet('/game/chat','friends.apply',{inviteId:code},function(err,res){
            cc.log('res:',res);
            if(!err){
                global.UI.ToolTip({message:res.msg});
            }
        })
    },

    updateUI(){
        this.content.removeAllChildren();
        for(var i=0; i<this.o_users.length; i++){
            var user = this.o_users[i]
            var node = cc.instantiate(this.requestItem);
            node.getComponent('UserInfo').setOptions(user)
            this.content.addChild(node);
            global.Common.addButtonHandler(this,cc.find('tongyong_shuru/delete',node),'onBtnIgnore',user._id);
            global.Common.addButtonHandler(this,cc.find('tongyong_shuru/apply',node),'onBtnApply',user._id);
        }
    },
    setOptions(options){
        this.o_users = options.usersInfo
        this.updateUI();
    },

    onBtnIgnore(event,_id){
        cc.log('不同意:',_id);
        this.doFriend(_id,0)
    },
    onBtnApply(event,_id){
        cc.log('同意:',_id);
        this.doFriend(_id,1)
    },
    doFriend(id,do1){
        this.httpGet('/game/chat','friends.deal',{_id:id,apply:do1},(err,res)=>{
            if(err){
                return
            }
            global.UI.ToolTip({message:res.msg});
            for(var i=0; i<this.o_users.length; i++){
                if(id == this.o_users[i]._id){
                    this.o_users.splice(i,1);
                    this.updateUI();
                    return;
                }
            }
        })
    },

    onExitWindowBegin(){
        this._super();
        cc.find('views/head/input',this.node).active = false;
    }
});
