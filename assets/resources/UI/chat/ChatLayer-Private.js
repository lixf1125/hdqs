
cc.Class({
    extends: require('wy-Component'),

    properties: {
        privateItem : cc.Prefab,
        content :cc.Node
    },
    onLoad(){
        this.node.on('onWindowFocus',this.onWindowFocus,this);
        this.b_showDelete = false;
    },
    onBtnPrivate(event,_id){
        cc.log('onBtnPrivate:',_id);
        var node = global.UIMgr.pushUI('UI/chat/PrivateChat')
        node.getComponent('PrivateChat').setOptions({_id:_id})
    },
    onBtnDeleteAll(){
        var self = this;
        var ids = '';
        for(var i=0; i<this.messages.length; i++){
            ids+=(i!=0?',':'')+this.messages[i].user._id
        }
        this.httpGet('/game/chat','privateChat.rmChatRecord',{_ids:ids},function(err,res){
            self.node.getChildByName('wuxiaoxi').active=true;
            self.node.getChildByName('scrollview').active=false;
            self.updateUI();
        })
    },
    onBtnDelete(){
        this.b_showDelete = !this.b_showDelete;
        for(var i=0; i<this.content.children.length; i++){
            var item = this.content.children[i];
            cc.find('tongyong_shuru/delete',item).active = this.b_showDelete;
        }
        this.node.getChildByName('alldelete').active = this.b_showDelete;
    },
    updateUI(){
        this.b_showDelete = false;
        this.content.removeAllChildren();
        for(var i=0; i<this.messages.length; i++){
            var msg = this.messages[i];
            var item = cc.instantiate(this.privateItem);
            item.getComponent('UserInfo').setOptions(msg.user);
            cc.find('tongyong_shuru/delete',item).active = false;
            cc.find('tongyong_shuru/message',item).getComponent(cc.Label).string = msg.message.content
            this.content.addChild(item);
            global.Common.addButtonHandler(this,item,'onBtnPrivate',msg.user._id);
            global.Common.addButtonHandler(this,cc.find('tongyong_shuru/delete',item),'onSelectDelete',msg.user._id);
        }
        this.node.getChildByName('alldelete').active = this.b_showDelete;
        this.node.getChildByName('delete').getComponent('UI-Toggle').setToggle(0)
    },
    onSelectDelete(event,id){
        cc.log('id:',id);
        var self = this;
        this.httpGet('/game/chat','privateChat.rmChatRecord',{_ids:[id]},function(err,res){
            for(var i=0; i<self.messages.length; i++){
                if(self.messages[i].user._id == id){
                    self.messages.splice(i,1);
                    self.updateUI();
                    break;
                }
            }
        })
    },
    onWindowFocus(){
        // this.updateUI();
    },

    onPageHide(){
        cc.log('private hide');
    },
    onPageShow(){
        cc.log('private show');
        var self = this;
        this.httpGet('/game/chat','privateChat.list',{},function(err,res){
            cc.log('私聊信息:',res.messages[0])
            if(res.messages[0]!=undefined){
                self.node.getChildByName('wuxiaoxi').active=false;
                self.node.getChildByName('scrollview').active=true;
                self.messages = res.messages;
                this.updateUI();
            }else{
                self.node.getChildByName('wuxiaoxi').active=true;
                self.node.getChildByName('scrollview').active=false;
            }
            
        }.bind(this))
    }
});
