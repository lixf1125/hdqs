
cc.Class({
    extends: require('ChatCommon'),

    properties: {
        chatItemGuild :cc.Prefab
    },

    onLoad () {
        this.setChannel('world');
        global.Event.on('newchatmsg',this.updateMessage.bind(this),this)
    }, 
    scrollToBottom(){
    },
    updateMessage(){
        this.messages = global.ChatMsgQueue
        this.addMessageToBottom();
    },
    onDestroy(){
        global.Event.off('newchatmsg')
        for(var i=0; i<global.ChatMsgQueue.length; i++){
            var msg = global.ChatMsgQueue[i];
            msg.isShow = false;
        }
    },

    instantiateItem(msg,lasttime,idx){
        var self = this;
        if(msg.type == 0){
            return this._super(msg,lasttime,idx)
        } else {
            var node = cc.instantiate(this.chatItemGuild);
            node.getComponent('UserInfo').setOptions(msg);
            node.isNew = !msg.isShow;

            var content = JSON.parse(msg.content);
            if(node.isNew){
                node.opacity = 0;  //先隐藏一下，否则会闪一下
            }
                
            if(msg.guild){
                this.updateGuildInfo(msg.guild,node);
            } else {
                node.opacity = 0;
                
                this.httpGet('/game/guild','getMessage',{guildId:content.guildId}, function(node,err,res){
                    if(!err){
                        this.guild = res
                        self.updateGuildInfo(this.guild,node);
                    }
                }.bind(msg,node));
            }
            
            if(msg.time-lasttime > 60*6000){
                var a = new Date(msg.time);
                var time = '';
                if(idx != 0){
                    var b = new Date(lasttime);
                    if(a.getFullYear() != b.getFullYear() || a.getMonth() != b.getMonth() || a.getDate() != b.getDate()){
                        time = a.getFullYear()+'-'+a.getMonth()+'-'+a.getDate()+' '
                    }
                } else {
                    time = a.getFullYear()+'-'+a.getMonth()+'-'+a.getDate()+' '
                }
                time += a.getHours()+a.getMinutes()+':'+a.getSeconds();
                cc.find('top/time',node).active = true;
                cc.find('top/time',node).getComponent(cc.Label).string =  time
            }
            cc.find('panel/message',node).getComponent(cc.Label).string = content.message;
            
            return node
        }
    },

    updateGuildInfo(guild,node){
        var headinfo = cc.find('panel/headinfo',node);
        headinfo.getChildByName('name').getComponent(cc.Label).string = guild.name;
        headinfo.getChildByName('count').getComponent(cc.Label).string = guild.userCount;

        cc.find('panel/id',node).getComponent(cc.Label).string = guild.id;
        

        cc.find('panel/join/level',node).getComponent(cc.Label).string = guild.leastCivilizationLevel;
    },

    sendMessage(){
        var self=this;
        this.sendObject({type:0,content:this.editText.string},function(err,res){
            if(!err){
                self.editText.string=''
                global.UI.ToolTip({message:'消息发送成功'})
            }
            global.Event.emit('requestWorldMessage');
        });
    },
    onPageShow(){
        this.updateMessage(); 
    }
});
