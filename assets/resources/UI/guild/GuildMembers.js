
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        memberItem : cc.Prefab,
        content : cc.Node,
        applyBtn : cc.Button
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._super();
        var self = this;
        this.httpGet('/game/guild','getUserList',{guildId:global.Data.guild._id}, function(err,res){
            if(!err){
                self.setOptions(res,global.Data.guild.selfLevel);
            }
        });
    },

    start () {

    },
    updateData(options,level){
        this.content.removeAllChildren();
        this.applyBtn.interactable = level >= 70
        var titleArray = {0:0,10:1,80:2,90:3,100:4}
        var hint = cc.find('views/head/apply/hint',this.node)
        hint.active = global.Data.guild.applyCount > 0;
        hint.getChildByName('label').getComponent(cc.Label).string = global.Data.guild.applyCount;

        options.sort(function(a,b){
            return a.level < b.level
        })
        var time = new Date().getTime();
        for(var i=0; i<options.length; i++){
            var user = options[i];
            var item = cc.instantiate(this.memberItem);
            item.getComponent('UserInfo').setOptions(user);
            if(time - user.lastLoginTick < 5*60*1000){
                cc.find('desc/other/line/active',item).getComponent(cc.Label).string = '在 线'
                cc.find('desc/other/offtime',item).active = false;
            } else {
                var t = time - user.lastLoginTick;
                cc.find('desc/other/offtime',item).getComponent(cc.Label).string = '离线'+Math.floor(t/60000)+'分钟'
            }
            cc.find('desc/head/icon',item).getComponent('UI-Toggle').setToggle(titleArray[user.level]);
            if(user._id == global.Data._id){
                item.getChildByName('up').active = false;
                item.getChildByName('down').active = false;
            } else if(user.level +10 >= level){
                // item.getChildByName('up').opacity = 0;
                item.getChildByName('up').active =false;
                item.getChildByName('down').active =false;
            } else if(user.level < 10 ){
                item.getChildByName('down').getChildByName('text').getComponent(cc.Label).string = '踢出'
                // item.getChildByName('down').opacity = 0;
                // item.getChildByName('down').getComponent(cc.Button).interactable = false;
            }
            // cc.find('')
            this.content.addChild(item);
            global.Common.addButtonHandler(this,item.getChildByName('up'),'onBtnUpLevel',user._id);
            if(user.level >= 10){
                global.Common.addButtonHandler(this,item.getChildByName('down'),'onBtnDownLevel',user._id);
            } else {
                global.Common.addButtonHandler(this,item.getChildByName('down'),'onBtnDownFack',user._id);
            }
               

        }
    },
    setOptions(options,level){
        this.o_options = options;
        this.n_level = level;
        this.updateData(this.o_options,level)
    },
    onBtnApplys(){
        this.httpGet('/game/guild','getApplyList',{}, function(err,res){
            if(!err){
                var members = global.EJMgr.pushUI('UI/guild/GuildApplys');
                members.getComponent('GuildApplys').setOptions(res);
            }
        });
    },
    onWindowFocus(){
        cc.log('onWindowFocus')
        var hint = cc.find('views/head/apply/hint',this.node)
        hint.active = global.Data.guild.applyCount > 0;
        hint.getChildByName('label').getComponent(cc.Label).string = global.Data.guild.applyCount;
    },
    onBtnUpLevel(event,id){
        cc.log('升职',id);
        this.updateLevel(id,1)
        
    },
    onBtnDownFack(event,id){
        var self = this;
        this.httpGet('/game/guild','removeUser',{userId:id}, function(err,res){
            if(!err){
                for(var i=0; i<self.o_options.length; i++){
                    if(id == self.o_options[i]._id){
                        self.o_options.splice(i,1);
                        self.updateData(self.o_options,self.n_level);
                        return;
                    }
                }
            }
        });
    },
    onBtnDownLevel(event,id){
        cc.log('降职',id);
        this.updateLevel(id,0);
    },
    updateLevel(id,do1){
        var self = this;
        this.httpGet('/game/guild','doLevel',{userId:id,do:do1}, function(err,res){
            if(!err){
                for(var i=0; i<self.o_options.length; i++){
                    if(id == self.o_options[i]._id){
                        self.o_options[i].level = res;
                        self.updateData(self.o_options,self.n_level);
                        return;
                    }
                }
            }
        });
    },

    onBtnRecruit(){
        global.EJMgr.pushUI('UI/guild/GuildRecruit')
    }
    // update (dt) {},
});
