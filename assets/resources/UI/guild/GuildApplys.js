
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        applyItem : cc.Prefab,
        content : cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
    },

    setOptions(options){
        for(var i=0; i<options.length; i++){
            var user = options[i];
            var node = cc.instantiate(this.applyItem);
            this.content.addChild(node);
            node.getComponent('UserInfo').setOptions(user);
            node.__id = user.applyId;
            global.Common.addButtonHandler(this,node.getChildByName('refuse'),'onBtnRefuse',user.applyId);
            global.Common.addButtonHandler(this,node.getChildByName('apply'),'onBtnApply',user.applyId);
        }
    },
    onBtnRefuse(event,d){
        cc.log('拒绝:',d);
        this.controlApply(d,0);
    },
    onBtnApply(event,d){
        cc.log('同意:',d);
        this.controlApply(d,1);
    },
    controlApply(_id,value){
        var self = this;
        this.httpGet('/game/guild','doApply',{applyId:_id,do:value},function(err,res){
            if(!err){
                global.Data.guild.applyCount++;
                for(var i=0; i<self.content.children.length; i++){
                    var node = self.content.children[i];
                    if(node.__id == _id){
                        self.content.removeChild(node);
                        return;
                    }
                }
            }
        })
    },
    


    // update (dt) {},
});
