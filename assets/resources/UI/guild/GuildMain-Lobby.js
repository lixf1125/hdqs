cc.Class({
    extends: require('wy-Component'),

    properties: {
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.level = null;
        // var self = this;
        // this.httpGet('/game/guild','getMessage',{}, function(err,res){
        //     if(!err)
        //         self.updateGuildInfo(res);
        // });
        // this.httpGet('/game/guild','getSelfMessage',{}, (err,res)=>{
        //     if(!err){
        //         // cc.log('getSelfMessage:',res)
        //         cc.find('coinbg/label',this.node).getComponent(cc.Label).string = res.self.coin
        //         self.level = res.self.level;
        //     }
        // });  
    },

    updateGuildInfo(guild){
        global.Data.guild = guild;
        cc.log('------',guild)
        var bg = cc.find('haidaobangxinxilan/bg2',this.node);
        cc.find('name/desc',bg).getComponent(cc.Label).string = guild.name
        cc.find('id/desc',bg).getComponent(cc.Label).string = guild.id
        cc.find('member/desc',bg).getComponent(cc.Label).string = guild.userCount
        cc.find('apply/desc',bg).getComponent(cc.Label).string = guild.joinPower == 0 ? '自由加入' : (guild.joinPower == 1?'需要验证':'禁止加入')
        cc.find('const/desc',bg).getComponent(cc.Label).string = guild.leastCivilizationLevel
        cc.find('active/desc',bg).getComponent(cc.Label).string = guild.weekActive
    },
    onBtnMember(){
        if(global.Data.guild.selfLevel != undefined){
            global.EJMgr.pushUI('UI/guild/GuildMembers');
        }
    },

    onBtnExitGuild(){
        this.httpGet('/game/gameUser','sociality.exitGuild',{},function(err,res){
            if(!err){
                global.Data.guildId = null;
                global.UIMgr.popUI();
            }
        })
    },

    start () {

    },

    // update (dt) {},
});
