
let colorArr = {
    blue: [0, 0, 255],
    red: [255, 0, 0],
    yellow: [255, 235, 4],
    green: [0, 255, 0],
    white: [0, 0, 0],
    black: [255, 255, 255],
};
cc.Class({
    extends: require('wy-Component'),

    properties: {
        buttonNode: cc.Node,
        textFont: cc.Label,
        _options: {
            default: {},
        }
    },

    onLoad: function () {
        this._options = {
            x: 0, y: 0,
            width: 100,
            height: 50,
            btnColor: 'blue',
            text: '确定',
            textColor: 'white',
            tapCallback: null,//按钮点击回调
        }
    },

    start: function () {
        this.buttonNode.on('touchend', this.tapBtn, this);        
    },

    //参数参考onLoad中的this._options
    setOptions: function (obj) {
        Object.assign(this._options, obj);
        this.buttonNode.setPosition(this._options.x, this._options.y);
        this.buttonNode.setContentSize(this._options.width, this._options.height);

        let btnColor = colorArr[this._options.btnColor];
        if (!btnColor) { btnColor = [0, 0, 255]; }
        this.buttonNode.color = new cc.Color(btnColor[0], btnColor[1], btnColor[2]);

        let textColor = colorArr[this._options.textColor];
        if (!textColor) { textColor = [0, 0, 255]; }
        this.textFont.node.color = new cc.Color(textColor[0], textColor[1], textColor[2]);
        this.textFont.string = this._options.text;
    },

    tapBtn: function () {
        if (this._options.tapCallback) {
            (this._options.tapCallback)();
        }
    },

    // update (dt) {},
});
