cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        
    },

    onLoad(){
        this._super();
        var btn_panel = cc.find('panel/btns',this.node);
        this.btns = [];
        var btnNames = ['rank','build','shop','friend','enemy','message','notice','setting']
        for(var i=0; i<btnNames.length; i++){
            var btn = btn_panel.getChildByName(btnNames[i])
            global.Common.addButtonHandler(this,btn,'onBtnClick',i);
            this.btns.push(btn)
        }
        this.lb_nickName = cc.find('panel/userInfo/top/nickName',this.node).getComponent(cc.Label);
        this.lb_id = cc.find('panel/userInfo/bottom/id',this.node).getComponent(cc.Label);
        this.js_imageUrl = cc.find('panel/userInfo/iconMask/avatar',this.node).getComponent('ImageLoader')


        global.Data.province = parseInt(global.Data.province) ? parseInt(global.Data.province) : -1
        global.Data.city = parseInt(global.Data.city) ? parseInt(global.Data.city) : -1
        global.Data.constellation = parseInt(global.Data.constellation) ? parseInt(global.Data.constellation) : -1
        this.updateInfo();

        global.Event.on('updateSelfInfo',this.updateInfo.bind(this))
    },

    onDestroy(){
        global.Event.off('updateSelfInfo',this.updateInfo.bind(this))
    },

    updateInfo(){
        var xingzuo = cc.find('panel/userInfo/xingzuo',this.node);
        xingzuo.getChildByName('icon').getComponent('UI-Toggle').setToggle(global.Data.constellation > 0 ? 1 :0)
        xingzuo.getChildByName('tag').getComponent(cc.Label).string = global.DataConfig.constellation[global.Data.constellation]
        var address = cc.find('panel/userInfo/address',this.node);
        address.getChildByName('icon').getComponent('UI-Toggle').setToggle(global.Data.province >= 0 ? 1 :0)

        var pro = '';
        if(global.Data.province >= 0){
            var i = 0;
            for(var k in global.DataConfig.provinceInfo){
                if(i == global.Data.province){
                    pro = k

                    if(global.Data.city >= 0){
                        pro+=global.DataConfig.provinceInfo[k][global.Data.city];
                    }
                    break;
                }
                i++
            }
            
        } else {
            pro = '未设置'
        }
        
        address.getChildByName('tag').getComponent(cc.Label).string = pro
    },

    onBtnClose(){
        global.UIMgr.popUI();
    },

    onBtnUserInfo(){
        global.EJMgr.pushUI('UI/myInfo/EditUserInfo')
    },

    onWindowFocus(){
        console.log('')
        this.updateInfo();
    },

    onBtnClick(event,data){
        cc.log('data:',data)
        global.UIMgr.popUI();
        var self = this;
        if(data == 8){
            global.Wechat.cleanResource();
        } else if(data == 0){
            global.UIMgr.loadLayer('Rank',0);
        }
        else if(data == 1){
            this.addExitMothod(function(){
                global.GameLoop.toBuildLayer()
            });
        }
        else if(data == 3){
            global.UIMgr.loadLayer('Chat',0);
        } else if(data == 4){

            this.httpGet('/game/chat','friends.list',{}, function(err,res){
                if(!err){
                    self.addExitMothod(function(){
                        var node = global.EJMgr.pushUI('prefab/mutual/FramFriends');
                        node.getComponent('FramFriends').setOptions(res.friends,'好友列表',function(){},'赠送');
                    });
                }
            });
        } else if(data == 5){
            global.UIMgr.loadLayer('Email',0)
        } else if(data == 6){
            if(global.Data.sysNotices.length){
                global.EJMgr.pushUI('UI/noticeLayer/NoticeLayer')
            } else {
                global.UI.ToolTip({message:'当前无公告'});
            }
        } else if(data == 7){
            global.UIMgr.loadLayer('Setting',0);
        }
    },

    onEnterWindowBegin(){
        this.node.getChildByName('bg').runAction(cc.fadeTo(0.15,180))
        var panel = this.node.getChildByName('panel');
        panel.stopAllActions();
        panel.x = -600;
        panel.runAction(cc.sequence(cc.moveTo(0.25,cc.v2(-360,0)),cc.moveTo(0.05,cc.v2(-370,0))))
        for(var i=0; i<this.btns.length; i++){
            var btn = this.btns[i]
            btn.opacity = 0;
            btn.scaleY = 0.3
            btn.runAction(cc.sequence(cc.delayTime(0.3+i*0.08),cc.spawn(cc.fadeIn(0.1),cc.scaleTo(0.2,1.0))));
        }
        this.lb_nickName.string = global.Data.nickname;
        this.lb_id.string = 'ID:'+global.Data.id;
        this.js_imageUrl.setOptions({url:global.Data.headImgUrl})

    },

    onExitWindowBegin(){
        this.node.getChildByName('bg').runAction(cc.fadeOut(0.1))
        var panel = this.node.getChildByName('panel');
        panel.stopAllActions();
        panel.x = -370;
        panel.runAction(cc.sequence(cc.moveTo(0.1,cc.v2(-360,0)),cc.moveTo(0.2,cc.v2(-950,0))))
    }
});
