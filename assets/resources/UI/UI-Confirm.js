const Loader = require('wy-Loader');
cc.Class({
    extends: require('wy-Component'),

    properties: {
        _options: {
            default: {}
        },
        alertNode: cc.Node,
        alertFont: cc.Label,
    },

    onLoad: function () {
        this.panel = Loader.getInstantiate('UI/UI-Panel');
        this.node.addChild(this.panel);
        this.okButton = Loader.getInstantiate('UI/UI-TextButton');
        this.cancelButton = Loader.getInstantiate('UI/UI-TextButton');
        this.alertNode.addChild(this.okButton);
        this.alertNode.addChild(this.cancelButton);
        this._options = {
            message: '随便写点什么显示出来就可以了，过程不重要！',
            title: 'Alert组件',
            okButtonText: "确认",
            cancelButtonText: "取消",
            okButtonCallback: () => {
                cc.log('点击了确定！');
            },
            cancelButtonCallback: () => {
                cc.log('点击了取消！');
            },
        }
    },

    start: function () {

    },

    setOptions: function (data) {
        Object.assign(this._options, data)
        this.alertFont.string = data.message;
        let oKButtonOptions = {
            x: 100, y: -100,
            width: 160,
            height: 60,
            btnColor: 'blue',
            textColor: 'black',
            text: this._options.okButtonText,
            tapCallback: () => {
                this._options.okButtonCallback();
                this.node.destroy();
            },
        }
        this.okButton.js.setOptions(oKButtonOptions);

        let cancelButtonOptions = {
            x: -100, y: -100,
            width: 160,
            height: 60,
            btnColor: 'yellow',
            textColor: 'black',
            text: this._options.cancelButtonText,
            tapCallback: () => {
                this._options.cancelButtonCallback();
                this.node.destroy();
            },
        }
        this.cancelButton.js.setOptions(cancelButtonOptions);


        let panelObj = {
            x: 0, y: 0,
            width: 600,
            height: 350,
            isClose: false,
            isModel: true,
            isModelClose: false,
            content: this.alertNode,
            titleText: this._options.title,
        };
        this.panel.js.setOptions(panelObj);
    },
    // update (dt) {},
});
