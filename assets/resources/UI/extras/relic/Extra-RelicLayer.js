
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        btnProps: [require('Prop')],
        revertTime :require('RevertTime')
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._super();
        this.no_views = this.node.getChildByName('views');
        this.no_bg = this.node.getChildByName('relicBg');
        this.no_points = this.no_views.getChildByName('points');
        this.initWindowSize();
        this.o_bag = {};
        this.o_data = null;
        this.b_isSelect = false;
        this.n_selectProp = 0;
        this.n_selectPoint = 0;
        this.o_give = {};

        this.getSelfInfo();
    },

    getSelfInfo(){
        var self = this;
        this.httpGet('/game/gameUser','relic.info',{},function(err,res){
            cc.log('',res);
            self.o_bag = res.bag
            self.o_data = res.relicData
            self.revertTime.startTimeRevert(res.endTime,function(){
                self.revertTime.node.getComponent(cc.Label).string = '当前活动未开启'
            });
            self.updateButton();
        })
    },

    updateButton(){
        var propconfig = global.DataConfig.extra.relic.prop;
        for(var i=0; i<propconfig.length; i++){
            var prop = global.Common.arrayMatchObject(this.o_bag,{id:propconfig[i]});
            this.btnProps[i].setId(propconfig[i],prop ? prop.count : 0)
        }
    },

    updateFangWei(ani){
        for(var i=0; i<6; i++){
            var no = this.no_points.getChildByName('item'+i);
            var sgnode;
            global.isVersion2 = cc.ENGINE_VERSION > '2.0.0';
            var sgnode;
            if(cc.ENGINE_VERSION > '2.0.0'){
                sgnode = no.getChildByName('icon').getComponent(cc.Sprite)
            }else{
                sgnode = no.getChildByName('icon').getComponent(cc.Sprite)._sgNode
            }
            
            var st = sgnode.getState()
            var propid = this.o_data.relicData[i].propId
            if(!ani){
                if(cc.ENGINE_VERSION > '2.0.0'){
                    no.getChildByName('icon').getComponent(cc.Sprite).setState(propid == 0?1:0);
                }else{
                    no.getChildByName('icon').getComponent(cc.Sprite)._sgNode.setState(propid == 0?1:0);
                }
               
            } else {
                if(st == 1 && propid > 0){
                    no.getChildByName('icon').runAction(cc.sequence(cc.blink(1,2),cc.callFunc(function(no){
                        this.setState(0)
                    }.bind(sgnode))))
                } else if(st == 0 && propid == 0){
                    no.getChildByName('icon').runAction(cc.sequence(cc.blink(1,2),cc.callFunc(function(no){
                        this.setState(1)
                    }.bind(sgnode))))
                }
            }
        }
    },

    switchIconToButton(blink){
        this.b_isSelect = false;
        for(var i=0; i<6; i++){
            var no = this.no_points.getChildByName('item'+i);
            no.stopAllActions();
            no.opacity = 255;
            blink && no.runAction(cc.repeatForever(cc.blink(1,2)))
        }
    },

    onBtnSelectFangWei(e,d){
        if(!this.b_isSelect){
            return
        }
        self.useProp(d)
    },

    addStore(st){
        cc.log('添加前:',global.Data.store)
        for(var i=0; i<st.length; i++){
            for(var k in st[i]){
                global.Data.store[k]+=st[i][k];
            }
            
        }
        cc.log('添加后:',global.Data.store)
        
    },

    useProp:function(pix){
        this.switchIconToButton(false);
        var self = this;
        this.httpGet('/game/gameUser','relic.useProp',{index:pix,propId:this.n_selectProp},function(err,res){
            global.UI.ToolTip({message:'使用物品成功'})
            for(var i=0; i<self.o_bag.length; i++){
                if(self.o_bag[i].id == self.n_selectProp){
                    self.o_bag[i].count -= 1
                }
            }
            self.updateButton();
            
            if(res.gives.length > 0){
                self.addStore(res.gives);
                for(var i=0; i<6; i++){
                    self.o_data.relicData[i].propId = 0;
                }
            } else {
                self.o_data.relicData[self.n_selectPoint-1].propId = self.n_selectProp;
            }
            
            self.updateFangWei(true);
        })
    },

    onBtnUse(event,d){
        cc.log('onBtnUse:',d)
        this.n_selectProp = global.DataConfig.extra.relic.prop[d];
        this.n_selectPoint = d;
        var p = event.target.convertToWorldSpace(cc.v2(0,event.target.height))
        
        var layer = global.EJMgr.pushUI('UI/extras/relic/ExtraSelectUse');
        var self = this;
        layer.js.setOptions(d == 0,p,function(cmd){
            global.EJMgr.popUI();
            if(cmd == 2){
                if(d == '0'){
                    this.b_isSelect = true;
                    self.switchIconToButton(true)
                } else {
                    self.useProp(global.DataConfig.extra.relic.prop[d]%10)
                }
            } else if(cmd == 1){
                
                self.o_give = {
                    props:global.DataConfig.extra.relic.prop[d], //可以为 0,0
                    counts :1
                }
                console.log('赠送:',self.o_give)
                self.showFriendInfo();
            } else {
                console.log('交换暂时不做')
                global.UI.ToolTip({message:'交换暂时不做'})
            }
        })
    },

    onBtnRecevie(){
        // var friend = global.EJMgr.pushUI('UI/extras/ExtraFriend');
        var self = this;
        this.httpGet('/game/gameUser','relic.giveHistory',{},function(err,res){
            var friend = global.EJMgr.pushUI('UI/extras/ExtraFriend');
            res.extra = 'relic'
            res.cb = self.getSelfInfo.bind(self);
            friend.js.setOptions(res)
        })
    },

    showFriendInfo(){
        var self = this;
        this.httpGet('/game/chat','friends.list',{}, function(err,res){
            if(!err){
                var node = global.EJMgr.pushUI('prefab/mutual/FramFriends');
                node.getComponent('FramFriends').setOptions(res.friends,'好友列表',self.onBtnSelectGive.bind(self),'赠送');
            }
        });
    },

    onBtnSelectGive(_id){
        this.o_give.toId = _id
        console.log('赠送:',this.o_give)
        var self = this
        this.httpGet('/game/gameUser','relic.give',this.o_give,function(err,res){
            if(!err){
                global.UI.ToolTip({message:'赠送成功'})
                self.getSelfInfo();
            }
        })
    },

    initWindowSize(){
        this.no_views.scale = 0.7;
        this.no_views.opacity = 0;
        this.no_bg.x = 720;
        for(var i=0; i<this.no_points.children.length; i++){
            var chd = this.no_points.children[i];
            chd.scale = 0.7;
            chd.opacity = 0;
        }
    },

    onEnterWindowBegin(){
        this.no_bg.runAction(cc.moveTo(0.2,cc.v2(0,0)))
        this.no_views.runAction(cc.sequence(cc.delayTime(0.2),cc.spawn(cc.scaleTo(0.2,1),cc.fadeIn(0.2)),cc.callFunc(this.onEnterWindowEnd.bind(this))))
        for(var i=0; i<this.no_points.children.length; i++){
            var chd = this.no_points.children[i];
            chd.runAction(cc.sequence(cc.delayTime(0.5+i*0.1),cc.spawn(cc.scaleTo(0.1,1.2),cc.fadeIn(0.1)),cc.scaleTo(0.05,1)));
        }
    },
    onEnterWindowEnd(){
        this.updateFangWei();
    },
    onExitWindowBegin(){
        this.no_bg.runAction(cc.sequence(cc.delayTime(0.3),cc.moveTo(0.1,cc.v2(-50,0)),cc.moveTo(0.2,cc.v2(720,0))))
        this.no_views.runAction(cc.spawn(cc.scaleTo(0.2,0.7),cc.fadeOut(0.2)))
    }

    // update (dt) {},
});
