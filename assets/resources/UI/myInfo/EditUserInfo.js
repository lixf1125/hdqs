var DropListMenu = require('DropListMenu')
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        signEdit :cc.EditBox,
        wechatEdit : cc.EditBox,
        sex : require('UI-Toggle'),
        shengMenu : DropListMenu,
        shiMenu :DropListMenu,
        xingzuoMenu : DropListMenu
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._super();
        this.province = [];
        for(var key in global.DataConfig.provinceInfo){
            this.province.push(key)
        }
    },

    start () {
        this.provinceId = -1;
        this.shengMenu.setMenuList(this.province,global.Data.province,'未设置');
        this.shengMenu.setSelectCallBack(this.onSelectProvince.bind(this))
        
        this.xingzuoMenu.setMenuList(global.DataConfig.constellation,global.Data.constellation,'未设置');
        if(-1 != global.Data.province){
            this.onSelectProvince(global.Data.province)
        } else {
            this.shiMenu.setMenuList(['未选择省份'],-1,'未设置');
        }
        if(global.Data.wxNumber.length > 0){
            this.wechatEdit.string = global.Data.wxNumber
        }
        var panel = cc.find('edit/panel',this.node)
        panel.js.setOptions(global.Data);

        var guildName = cc.find('edit/panel/guild/text',this.node).getComponent(cc.Label);
        var icon = this.node.getChildByName()
        this.httpGet('/game/guild','getSelfMessage',{}, (err,res)=>{
            if(!err){
                guildName.string = res.name;
            }
        }); 
        cc.find('edit/panel/shuru/input',this.node).getComponent(cc.EditBox).string = global.Data.signature

    },
    onSelectProvince(idx){
        this.provinceId = idx;
        this.cityData = global.DataConfig.provinceInfo[ this.province[idx]];
        // Object.assign(this.cityData,ProvinceData[idx])
        // this.cityData.splice(0,1);
        this.shiMenu.setMenuList(this.cityData,global.Data.city,'未设置');
    },

    onToggle(toggle){
        cc.log('onToggle:',toggle.isChecked)
    },

    onEnterWindowBegin(){
        var node = this.node.getChildByName('edit');
        node.scale = 0.7
        node.runAction(cc.sequence(this.getEjectAction()))
        // cc.find('edit/avatar',this.node).getComponent('ImageLoader').setOptions({url:global.Data.headImgUrl})
    },
    onExitWindowBegin(){
        this.node.getChildByName('bg').runAction(cc.fadeOut(0.2))
        this.node.getChildByName('edit').runAction(cc.spawn(cc.scaleTo(0.2,0.7),cc.fadeOut(0.2)))
        this.wechatEdit.node.runAction(cc.spawn(cc.scaleTo(0.2,0.7),cc.fadeOut(0.2)))

        var obj = {};
        var idx = this.shengMenu.getIndex()
        if(idx != global.Data.province){
            obj.province = idx
        }
        idx = this.shiMenu.getIndex()
        if(idx != global.Data.city){
            obj.city = idx
        }
        idx = this.xingzuoMenu.getIndex()
        if(idx != global.Data.constellation){
            obj.constellation = idx
        }
        if(global.Data.wxNumber != this.wechatEdit.string){
            obj.wxNumber = this.wechatEdit.string
        }
        if(global.Data.signature != this.signEdit.string){
            obj.signature = this.signEdit.string
        }
        if(Object.keys(obj).length > 0){
            this.httpGet('/game/gameUser','editInfo',obj,function(err,res){
                cc.log('editInfo:',res)
                Object.assign(global.Data,obj);
                global.Event.emit('updateSelfInfo')
            })
        }
    }

    // update (dt) {},
});
