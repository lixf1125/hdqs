
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        icon : cc.Sprite,
        nickName :cc.Label,
        level : cc.Label,
        qianming: cc.Label,
        address : cc.Label,
        constellation : cc.Label,
        weid : cc.Label,
        guildIcon : require('UI-Toggle'),
        guildName : cc.Label,
        deleteBtn : cc.Button
    },

    onBtnClick(event,d){
        var self =this;
        if(d == 1){
            if(this.nexus == 1){
                self.httpGet('/game/chat','friends.remove',{_id:this.options._id},function(err,res){
                    if(!err){
                        global.UI.TooleTip({message:'删除好友成功'})
                        self.updateNexus(0)
                    }
                })
            } else if(this.nexus == 0){
                this.httpGet('/game/chat','friends.apply',{inviteId:this.options.id},function(err,res){
                    cc.log('res:',res);
                    if(!err){
                        global.UI.ToolTip({message:res.msg});
                        self.deleteBtn.interactable = false
                    }
                })
            }
        } else if(d == 0){
            console.log('没有做此项')
            // global.UIMgr.popUI();
            // var node = global.UIMgr.pushUI('UI/chat/PrivateChat')
            // node.getComponent('PrivateChat').setOptions({_id:this.n_id})
        } else if(d == 2){
            global.EJMgr.popUI();
            var node = global.UIMgr.pushUI('UI/chat/PrivateChat')
            node.getComponent('PrivateChat').setOptions({_id:this.n_id})
        }
    },

    updateNexus(nexus){
        this.nexus = nexus;
        if(nexus == 2){
            this.deleteBtn.interactable = false;
            this.deleteBtn.node.getChildByName('Label').string = '仇人';
        } else if(nexus == 0){
            this.deleteBtn.node.getChildByName('Label').string = '好友申请';
        }
    },
    updateInfo(options,nexus){
        this.updateNexus(nexus)
        this.options = options
        options.nickname && this.nickName && (this.nickName.string = options.nickname);
        options.civilizationLevel != null && this.level && (this.level.string = options.civilizationLevel);
        var address = '';
        if(options.province && options.province.length > 0){
            var i = 0;
            for(var k in global.DataConfig.provinceInfo){
                if(i == options.province){
                    address = k

                    if(options.city && options.city.length > 0){
                        address+=global.DataConfig.provinceInfo[k][options.city];
                    }
                    break;
                }
                i++
            }
            
        } else {
            address = '未设置'
        }
        this.address && (this.address.string = address);
        options.sex && this.sex && (this.sex.setToggle(options.sex ? 1 : 0));
        this.qianming.string  = (!!options.signature ? options.signature : '未设置' )
        this.constellation && (this.constellation.string = (options.constellation.length > 0 ? global.DataConfig.constellation[options.constellation] : '未设置'));
        this.weid && (this.weid.string = (options.wxNumber.length > 0 ?options.wxNumber:'未设置'));
        // options.guild && options.guild.name && this.guildName && (this.guildName.string = options.guild.name);

        if(options.headImgUrl && this.icon){
            cc.loader.load({url:options.headImgUrl,type:'png'},function(err,tex){
                if(!err){
                    this.icon.spriteFrame = new cc.SpriteFrame(tex);
                }
            }.bind(this))
        }
        this.guildIcon.setToggle(options.guild&&options.guild.icon?options.guild.icon+1:0)
        this.guildName.string = options.guild&&options.guild.name?options.guild.name : '未加入';
    },
    setOptions(options){
        this.n_id = options._id;
        this.onClick = options.onClick;
        this.httpGet('/game/gameUser/getInfoById',{},{_id:options._id},function(err,res){
            if(!err)
                this.updateInfo(res.user,res.nexus);
        }.bind(this))

    }
});
