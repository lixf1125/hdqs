
cc.Class({
    extends: require('UI-PanelWindow'),

    properties: {
        pageView : cc.PageView,
        dogAtlas : [cc.SpriteAtlas] 
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._super();
        this.tg_left = cc.find('views/left',this.node).getComponent('UI-Toggle');
        this.tg_right = cc.find('views/right',this.node).getComponent('UI-Toggle');
        this.lb_name = cc.find('views/name',this.node).getComponent(cc.Label);
        this.no_bg = this.node.getChildByName('bg')
        this.no_bg.opacity = 0;
        this.no_views = this.node.getChildByName('views');
        this.lb_food = cc.find('views/food/count',this.node).getComponent(cc.Label);
        this.lb_expWater = cc.find('views/exp/count',this.node).getComponent(cc.Label);
        this.bt_battle = cc.find('views/battle',this.node).getComponent(cc.Button);
        this.lb_level = cc.find('views/info/level',this.node).getComponent(cc.Label);
        this.pro_exp = cc.find('views/info/progress',this.node).getComponent(cc.ProgressBar);
        this.lb_exp = cc.find('views/info/exp',this.node).getComponent(cc.Label);
        this.lb_skillDesc = cc.find('views/info/skilldesc',this.node).getComponent(cc.Label);
        this.lb_skillCurrent = cc.find('views/info/current',this.node).getComponent(cc.Label);
        this.lb_skillNext = cc.find('views/info/next',this.node).getComponent(cc.Label);
        this.sp_skillIcon = cc.find('views/info/skillicon',this.node).getComponent(cc.Sprite);
        this.js_time = cc.find('views/time',this.node).getComponent('LastTimeRevert');
        this.bt_uplevel = cc.find('views/info/uplevel',this.node).getComponent(cc.Button);
        // this.lb_skill = 

        // this.no_views.opacity = 0;
        this.no_views.x = 720
        this.n_currentIndex = 0;

        // var self = this;
        // this.httpGet('/game/gameuser','pet.getPetMessage',{}, function(err,res){
        //     cc.log('res:',res);
        //     if(!err){
        //         global.Data.PetData = res;
        //         self.updateData();
        //     } else {
        //         global.UI.ToolTip({message:'当前没有宠物'})
        //     }
        // })
    },

    start(){
        

        this.updateData();
    },

    onViewPage(){
        this.n_currentIndex = this.pageView.getCurrentPageIndex()
        this.updateData();
    },

    onBtnChange(event,data){
        this.n_currentIndex = this.pageView.getCurrentPageIndex()+parseInt(data)
        this.pageView.scrollToPage(this.n_currentIndex);
        
        this.updateData();
    },

    onBtnBattle(){
        cc.log('宠物上阵')
        if(this.n_currentIndex != global.Data.PetData.pet.currentIndex){
            var index = this.n_currentIndex
            
            this.httpGet('/game/gameuser','pet.battle',{petId:this.n_currentIndex}, function(err,res){
                global.Data.PetData.pet.currentIndex = index;
                this.updateData();
            }.bind(this))
        }
    },
    onBtnUpdateLevel(){
        var pet = global.Data.PetData.pet.data[this.n_currentIndex];
        if(!pet){
            cc.warn('宠物数据不存在:',this.n_currentIndex,global.Data.PetData.pet)
            return;
        }
        var petConfig = global.DataConfig.pets[pet.id];
        var needExp = petConfig.exp[pet.level]
        var self = this
        this.httpGet('/game/gameuser','pet.upgrade',{petId:this.n_currentIndex}, function(err,res){
            
            pet.exp -= needExp;
            pet.level ++;

            this.updateData();
        }.bind(this))
    },

    onBtnUse(event,data){
        var idx = this.n_currentIndex
        this.httpGet('/game/gameuser','pet.feed',{type:parseInt(data),petId:this.n_currentIndex,typeNo:0}, function(err,res){
            if(err){
                return;
            }
            if(data == '1'){
                global.Data.PetData.pet.data[idx].exp = res.exp;
            } else {
                global.Data.PetData.pet.data[idx].hungryTime += res.hungryTime;
            }
            this.updateData();
        }.bind(this))
    },

    updateData(){
        if(!global.Data.PetData){
            return;
        }
        var data = global.Data.PetData;
        this.lb_food.string = data.dogFood;
        this.lb_expWater.string = data.expWater;

        this.tg_left.setToggle(this.n_currentIndex == 0)
        this.tg_right.setToggle(this.n_currentIndex == 1)

        
        var pet = global.Data.PetData.pet.data[this.n_currentIndex];
        if(!pet){
            cc.warn('宠物数据不存在:',this.n_currentIndex,global.Data.PetData.pet)
            this.bt_battle.node.getChildByName('Label').getComponent(cc.Label).string = '未解锁';
            this.bt_battle.interactable = false;
            return;
        }
        this.bt_battle.node.getChildByName('Label').getComponent(cc.Label).string = this.n_currentIndex == global.Data.PetData.pet.currentIndex?'已上阵':'上阵'
        var petConfig = global.DataConfig.pets[pet.id];
        this.lb_level.string = pet.level;
        this.lb_name.string = petConfig.name
        this.pro_exp.progress = pet.exp >= petConfig.exp[pet.level] ? 1 :  pet.exp / petConfig.exp[pet.level];
        this.lb_exp.string = pet.exp+'/'+petConfig.exp[pet.level]
        this.bt_uplevel.interactable = pet.exp >= petConfig.exp[pet.level]
        var skillConfig = global.DataConfig.skill[pet.id];
        this.lb_skillDesc.string = skillConfig.desc;
        this.lb_skillCurrent.string = '当前机率:'+skillConfig.level[pet.level];
        if(pet.level < skillConfig.level.length-1){
            this.lb_skillNext.string = '下一级机率:'+skillConfig.level[pet.level+1];
        } else { 
            this.lb_skillNext.node.active = false;
        }
        this.js_time.setTime(pet.hungryTime)

        let vid = this.pageView.getCurrentPageIndex()
        var page = this.pageView.getPages()[vid];
        var dog = page.getChildByName('dog').getComponent('SpriteAnimate');
        if(pet.hungryTime > new Date().getTime()){
            dog.loadFrames(this.dogAtlas[0],'shouwei_',1,6);
            dog.setDelayTime(0.15);
            dog.startAnimate()
        } else {
            dog.loadFrames(this.dogAtlas[0],'jie_',1,9);
            dog.setDelayTime(0.1);
            dog.startAnimate()
        }
    },

    onEnterWindowBegin(){
        this.no_bg.runAction(cc.fadeTo(0.1,180));
        this.no_views.runAction(cc.moveTo(0.3,cc.v2(0,0)))
    },
    onExitWindowBegin(){
        this.no_bg.runAction(cc.fadeOut(0.1,0));
        this.no_views.runAction(cc.moveTo(0.3,cc.v2(720,0)))
    }

    // update (dt) {},
});
